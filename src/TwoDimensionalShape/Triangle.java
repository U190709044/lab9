package TwoDimensionalShape;

import Shapes.Shape;

public class Triangle extends Shape {

    public double side1;
    public double side2;
    public double side3;
    public double height;

    public Triangle(double side1, double side2, double side3, double height) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.height = height;
    }

    public double perimeter() {
        return side1 +side2+side3 ;
    }

    public double area(){
        return (side1 *height)/2;
    }
    
    public String toString(){
        return "Triangle Side 1: "+ side1 +" Triangle Side 2: "+side2+" Triangle Side 3: "+side3+" Triangle Height: "+height;
    }
}

