package TwoDimensionalShape;


import Shapes.Shape;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class Circle  extends Shape {

    public double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public double perimeter(){ return 2*Math.PI*radius; }

    public double area(){
        return Math.PI*radius*radius;
    }

    public String toString(){ return "Circle Radius: "+radius; }
}
