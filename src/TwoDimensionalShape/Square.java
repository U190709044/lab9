package TwoDimensionalShape;


import Shapes.Shape;

public class Square extends Shape {

    public double side;

    public Square(double side) { this.side = side; }

    public double perimeter(){ return side*4; }

    public double area(){
        return side*side;
    }

    public String toString(){
        return "Square Side: "+side;
    }

}