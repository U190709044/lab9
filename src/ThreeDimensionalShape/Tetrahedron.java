package ThreeDimensionalShape;

import Shapes.Shape;

public class Tetrahedron extends Shape {

    public double side;

    public Tetrahedron(double side) { this.side = side; }

    public double area(){ return side*side*Math.sqrt(3); }

    public double volume(){ return Math.pow(side,3)*Math.sqrt(2)/12; }
    public double perimeter(){ return side*6; }

    public String toString() { return "Tetrahedron Side: " + side ; }
}