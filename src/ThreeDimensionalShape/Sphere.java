package ThreeDimensionalShape;

import TwoDimensionalShape.Circle;

public class Sphere extends Circle {

    public Sphere(double radius) { super(radius); }
    public double area(){ return 4*super.area(); }
    public double volume(){ return 4*Math.PI*Math.pow(radius,3)/3; }
    public  double perimeter(){ return  2*Math.PI*radius; }
    public String toString(){ return "Sphere Radius: "+radius; }
}
