package ThreeDimensionalShape;


import TwoDimensionalShape.Square;

public class Cube extends Square {

    public Cube(double side) {
        super(side);
    }


    public double area(){
        return 6*super.area();
    }
    public double perimeter() {
        return (super.perimeter() * 3);
    }

    public double volume()
    {
        return (super.area()*side);
    }


    public String toString(){
        return "Cube Side: "+ side;
    }


}
