import Shapes.Shape;
import ThreeDimensionalShape.Cube;
import ThreeDimensionalShape.Sphere;
import ThreeDimensionalShape.Tetrahedron;
import TwoDimensionalShape.Circle;
import TwoDimensionalShape.Square;
import TwoDimensionalShape.Triangle;
import java.io.*;
import java.util.ArrayList;

public class Tester {
    public static void main(String[] args) throws IOException {
        ArrayList<Shape> shapes = new ArrayList<Shape>();
        FileReader fr = new FileReader("instructor.txt");
        BufferedReader br = new BufferedReader(fr);
        while (br.ready()) {
            String line = br.readLine();
            String[] factor = line.split(" ");
            if (factor[0].equals("O") && factor[1].equals("C")) {
                double radius = Double.parseDouble(factor[2]);
                 Shape shape = new Circle(radius);
                shapes.add(shape);
                System.out.println(shape);
            }
            else if (factor[0].equals("O") && factor[1].equals("S")) {
                double side = Double.parseDouble(factor[2]);
                Shape shape = new Square(side);
                shapes.add(shape);
                System.out.println(shape);
            }
            else if (factor[0].equals("O") && factor[1].equals("T")) {
                double base = Double.parseDouble(factor[2]);
                double side1 = Double.parseDouble(factor[3]);
                double side2 = Double.parseDouble(factor[4]);
                double height = Double.parseDouble(factor[5]);
                Shape shape = new Triangle(base, side1, side2, height);
                shapes.add(shape);
                System.out.println(shape);
            }
            else if (factor[0].equals("O") && factor[1].equals("SP")) {
                double radius = Double.parseDouble(factor[2]);
                Shape shape = new Sphere(radius);
                shapes.add(shape);
                System.out.println(shape);
            }
            else if (factor[0].equals("O") && factor[1].equals("CU")) {
                double side = Double.parseDouble(factor[2]);
                Shape shape = new Cube(side);
                shapes.add(shape);

                System.out.println(shape);
            }
            else if (factor[0].equals("O") && factor[1].equals("TE")) {
                double side = Double.parseDouble(factor[2]);
                Shape shape = new Tetrahedron(side);
                shapes.add(shape);
                System.out.println(shape);
            }
            else if (factor[0].equals("TA")) {
                double totalarea = 0;
                for (Shape shape :shapes) {
                    totalarea +=shape.area() ;
                }
                System.out.println("Total Area: "+ totalarea);
            }
            else if (factor[0].equals("TP")) {
                double totalperimeter = 0;
                for (Shape shape :shapes) {
                    totalperimeter += shape.perimeter();
                }
                System.out.println("Total Perimeter: "+totalperimeter);
            }
            else if (factor[0].equals("TV")) {
                double totalvolume = 0;
                for (Shape shape :shapes) {
                    totalvolume += shape.volume();
                }
                System.out.println("Total Volume: "+ totalvolume);
            }
        }
    }
}